<?php
/**
 * @package gravity-forms-onedrive-add-on
 * @version 1.0.3
 */
/*
Plugin Name: Gravity Forms OneDrive Addon-on
Plugin URI: https://bitbucket.org/bradmkjr/gravity-forms-onedrive-add-on
Description: This is not just a plugin, it symbolizes the hope and enthusiasm of an idiot.

Version: 1.0.3
Bitbucket Plugin URI: https://bitbucket.org/bradmkjr/gravity-forms-onedrive-add-on

Author: Bradford Knowlton
Author URI: http://bradknowlton.com/
*/

// This just echoes the chosen line, we'll position it later
function hello_world() {
	echo "<p id='world'>Hello World</p>";
}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'hello_world' );